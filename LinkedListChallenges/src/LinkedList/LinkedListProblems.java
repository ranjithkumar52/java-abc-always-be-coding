package LinkedList;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;
import java.util.Scanner;

public class LinkedListProblems {

    private LinkedList problem;
    private int stackSize;

    LinkedListProblems() {
        this.problem = new LinkedList();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the stack");
        stackSize = sc.nextInt();
        this.initLinkedList();

    }

    public LinkedList getLinkedList() {
        return problem;
    }

    public int getStackSize() {
        return stackSize;
    }

    public void setStackSize(int size) {
        stackSize = size;
    }

    //random assigning of values in the linkedList
    public void initLinkedList() {
        try {
            //fill out the stack
            Random rand = new Random();
            for (int i = 0; i < this.getStackSize(); i++) {
                this.stackPush(rand.nextInt(10) + 1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void stackPush(int data) throws Exception {

        if (isStackFull()) {
            throw new Exception("Can't push full stack");
        } else {
            problem.addFirst(data);
            //display();
        }

    }

    public int stackPop() throws Exception {
        if (isStackEmpty()) {
            throw new Exception("can't pop empty stack");

        } else {
            return (int) problem.removeFirst();

        }

    }

    public int stackTop() {
        return (int) problem.peekFirst();
    }

    public int stackSize() {
        return (int) problem.size();
    }

    public boolean isStackEmpty() {
        return problem.isEmpty();
    }

    public boolean isStackFull() {
        return stackSize() == stackSize;
    }

    public void displayString() {
        System.out.println("Elements in the stack: \n" + problem.toString());
    }
    
    public void displayIterator(){
        for(Object i: this.getLinkedList()){
            System.out.println(i);
        }
    }

    //problem 2: find nth node from the end of a linkedlist
    public int findNodeFromTheEnd() throws Exception {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the index to find the node from the end of the linkedList");
        int index = sc.nextInt();

        if (index >= 0 && index <= stackSize) {
            return (int) problem.get(stackSize - index);
        } else {
            throw new Exception("Index out of bounds");
        }

    }

    //problem 3: sort the linkedlist
    public void sortLinkedList() {
        Collections.sort(problem);
        System.out.println("LinkedList after sorting");
        displayString();
    }

    //reverse linkedList
    public void reverseLinkedList() {
        Collections.reverse(problem);
    }

    //copy source linkedList to another.
    public void copyLinkedList(LinkedListProblems source, LinkedListProblems dest) {
        Collections.copy(dest.getLinkedList(), source.getLinkedList());
    }
    
    public void iterateLinkedList(){
        ListIterator<LinkedList> namesIterator = this.getLinkedList().listIterator();
        while(namesIterator.hasNext()){
	   System.out.println(namesIterator.next());			
	}	

    }

}
