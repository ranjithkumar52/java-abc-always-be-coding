package arraylistchallenge;

import java.util.ArrayList;

public class MobilePhone {

    private ArrayList<Contact> contactBook = new ArrayList<>();

    public ArrayList<Contact> getContact() {
        return contactBook;
    }

    public void setContact(ArrayList<Contact> contact) {
        this.contactBook = contact;
    }

    public void storeContact(Contact newContact) {
        if (contactBook.add(newContact)) {
            System.out.println("Contact name: " + newContact.getName() + "\n"
                    + "Contact's phone number: " + newContact.getPhoneNumber() + "\n"
                    + " - added succesfully");
        } else {
            System.out.println("Failed to add the given contact");
        }
    }

    //public void modifyContact(Contact oldContact, Contact newContact) { //should use this more
    public void modifyContact(String oldName, String newName) {
        Contact newContact = findContact(oldName);
        if (newContact != null) {
            newContact.setName(newName);
            System.out.println("Updated succesfully");
        } else {
            System.out.println("Failed to modify");
        }
    }

    public void removeContact(String removeName) {
        Contact removeContact = findContact(removeName);
        if (removeContact != null) {
            if (contactBook.remove(removeContact)) {
                System.out.println("Removed contact succesfully");
            } else {
                System.out.println("Failed to remove contact");
            }
        }
    }

    private Contact findContact(String oldName) {
        int size = contactBook.size();
        for (int i = 0; i < size; i++) {
            if (contactBook.get(i).getName().equals(oldName)) {
                return contactBook.get(i);
            }
        }
        return null;
    }

    public void queryContact(String findName) {
        Contact indexContact = findContact(findName);
        if (indexContact != null) {
            System.out.println("Phone number of " + findName + " : " + indexContact.getPhoneNumber());

        } else {
            System.out.println("Failed to find the number");
        }
    }

    public void printContacts() {
        if (contactBook.isEmpty()) {
            System.out.println("Contacts are empty");
        } else {
            for (int i = 0; i < contactBook.size(); i++) {
                System.out.println("Contact Name: " + contactBook.get(i).getName() + " Contact Number: " + contactBook.get(i).getPhoneNumber());
            }
        }

    }

}
