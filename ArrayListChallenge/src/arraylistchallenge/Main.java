package arraylistchallenge;

import java.util.Scanner;

public class Main {

    private static MobilePhone mobilePhone = new MobilePhone();
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        boolean quit = false;
        while (!quit) {
            printInstructions();
            System.out.println("Enter your command");
            int command = scan.nextInt();
            scan.nextLine();
            switch (command) {
                case 1:
                    printContacts();
                    break;
                case 2:
                    addNewContact();
                    break;
                case 3:
                    updateExistingContact();
                    break;
                case 4:
                    removeContact();
                    break;
                case 5:
                    findContact();
                    break;
                case 6:
                default:
                    quit = true;
                    System.out.println("Exiting...");
                    break;
            }
        }
    }

    public static void printInstructions() {
        System.out.println("Choose");
        System.out.println("\t 1. Print all contacts");
        System.out.println("\t 2. Add a new contact");
        System.out.println("\t 3. Update an existing contact");
        System.out.println("\t 4. Remove a contact");
        System.out.println("\t 5. Find a contact");
        System.out.println("\t 6. Exit");
    }

    public static void printContacts() {
        mobilePhone.printContacts();
    }

    public static void updateExistingContact() {
        System.out.println("Enter the name of the contact to update");
        String oldName = scan.nextLine();

        System.out.println("Enter new name to change");
        String newName = scan.nextLine();

        mobilePhone.modifyContact(oldName, newName);

    }

    public static void removeContact() {
        printContacts();
        System.out.println("Enter the contact's name to be removed from the list above");
        String removeName = scan.nextLine();

        mobilePhone.removeContact(removeName);

    }

    public static void findContact() {
        System.out.println("Enter contact's name to find their phone number");
        String name = scan.nextLine();

        mobilePhone.queryContact(name);

    }

    public static void addNewContact() {
        System.out.println("Enter name of the contact");
        String name = scan.nextLine();

        System.out.println("Enter contact's phone number");
        int phoneNumber = scan.nextInt();
        scan.nextLine();

        Contact contact = new Contact(name, phoneNumber);
        mobilePhone.storeContact(contact);

    }

}
