package ArrayChallenges;

public class Main {

    public static void main(String[] a) {
        
        ArrayChallengesClass arrayObject1 = new ArrayChallengesClass();
        int[] newArray = arrayObject1.initArray();
        //arrayObject1.printArray(newArray, "Given Array");
        
        int uniqueCountSize = arrayObject1.countUniqueElementsInArray(newArray);
        System.out.println("uniqueCountSize = " + uniqueCountSize);
        

        //ArrayChallengesClass.removeDuplicates(someArray);
        //System.out.println("Average of the elements in the array: \n" + ArrayChallengesClass.averageOfTheArrayElements(someArray));

    }

}
