package ArrayChallenges;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class ArrayChallengesClass {

    private static Scanner scan = new Scanner(System.in);

    public double averageOfTheArrayElements(int[] averageArray) {
        //displayArray(averageArray);
        int sum = 0;
        for (int i : averageArray) {
            sum += i;
        }
        //NOTE: dont forget the cast
        return (double) sum / (double) averageArray.length;
    }

    public int[] removeDuplicates(int[] duplicateArray) {
        int[] sortedArray = sortArray(duplicateArray);
        printArray(sortedArray, "Sorted Array...");

        //Arraylist to store unique elements
        ArrayList<Integer> uniqueArrayList = new ArrayList<Integer>();
        //Store first element
        uniqueArrayList.add(duplicateArray[0]);

        //Code block to add unique elements from a duplicate array to arrayList     
        System.out.println("\nComparing...");
        for (int currentElement = 0; currentElement < duplicateArray.length - 1; currentElement++) {
            if (duplicateArray[currentElement] != duplicateArray[currentElement + 1]) {
                uniqueArrayList.add(duplicateArray[currentElement + 1]);
            }

        }

        int[] uniqueArray = new int[uniqueArrayList.size()];
        for (int i = 0; i < uniqueArrayList.size(); i++) {
            uniqueArray[i] = uniqueArrayList.get(i);
        }
        return uniqueArray;

    }

    public int[] getIntegers(int capacity) {
        int[] newArray = new int[capacity];
        System.out.println("Enter the elements in the console");
        for (int i = 0; i < capacity; i++) {
            newArray[i] = scan.nextInt();
        }
        return newArray;
    }

    public void printArray(int[] showArray, String s) {
        System.out.println(s);
        for (int index : showArray) {
            System.out.print(index + " ");
        }
        System.out.println();
    }

    public void printArray(int[] showArray) {
        for (int index : showArray) {
            System.out.print(index + " ");
        }
        System.out.println();
    }

    public int[] sortArray(int[] unsortedArray) {
        for (int currentElement = 0; currentElement < unsortedArray.length; currentElement++) {
            for (int nextElement = currentElement + 1; nextElement < unsortedArray.length; nextElement++) {
                if (unsortedArray[currentElement] < unsortedArray[nextElement]) {
                    unsortedArray[currentElement] += unsortedArray[nextElement];
                    unsortedArray[nextElement] = unsortedArray[currentElement] - unsortedArray[nextElement];
                    unsortedArray[currentElement] = unsortedArray[currentElement] - unsortedArray[nextElement];
                }
            }
        }
        int[] sortedArray = unsortedArray;
        return sortedArray;
    }

    public int[] initArray() {
        Random rand = new Random();
        int arraySize = rand.nextInt(10) + 1;
        int[] someArray = new int[arraySize];

        for (int index = 0; index < someArray.length; index++) {
            someArray[index] = rand.nextInt(20) + 1;
        }
        printArray(someArray, "Array Initilized...");
        return someArray;
    }
    
    public int countUniqueElementsInArray(int[] redudantArray){
        int[] uniqueArray = removeDuplicates(redudantArray);
        printArray(uniqueArray, "Unique elements in the array...");
        return uniqueArray.length;
    }
}
