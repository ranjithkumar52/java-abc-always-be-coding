package remote;

import devices.Device;

public class RemoteControl {

    boolean isBatteryLive = true;

    
    //connecting to multiple devices
    private Device device = null;

    //single remote control
    private static final RemoteControl oneSingleRemote = new RemoteControl();

    public void connectDevice(Device connectDevice) {
//        connectDevice.doDeviceWork();
//        System.out.println("Connecting to "+device+"....");
        
        this.device = connectDevice;
        device.doDeviceWork();
        System.out.println("Connecting to " + device + "....");
    }

    public static RemoteControl getInstance() {
        return oneSingleRemote;
    }

}
