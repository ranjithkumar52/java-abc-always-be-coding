package main;

import devices.*;
import remote.RemoteControl;

public class Human {

    public static void main(String[] args) {
        RemoteControl rc = RemoteControl.getInstance();
        
        Device projector = new Projector(2000);
        Device tv = new TV(8000);
        Device soundsystem = new SoundSystem(1500);
        
//        RemoteControl.connectDevice(soundsystem);
//        RemoteControl.connectDevice(tv);
//        RemoteControl.connectDevice(projector);

//      Using one single remote control object connect to multiple devices
        rc.connectDevice(soundsystem);
        rc.connectDevice(tv);
        rc.connectDevice(projector);
        
    }
}
