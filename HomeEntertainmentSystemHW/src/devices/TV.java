package devices;

public class TV extends Device {

    public TV(int costOfDevice) {
        super(costOfDevice);
    }

    @Override
    public void doDeviceWork() {
        System.out.println("TV is working...");
        this.initDevice(); 
    }

}
