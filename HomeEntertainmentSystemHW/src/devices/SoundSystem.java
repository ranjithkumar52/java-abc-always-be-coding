package devices;

public class SoundSystem extends Device {

    public SoundSystem(int costOfDevice) {
        super(costOfDevice);
    }

    @Override
    public void doDeviceWork() {
        System.out.println("sound system is working...");
        this.initDevice();
    }

}
