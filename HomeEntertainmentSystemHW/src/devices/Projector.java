package devices;

public class Projector extends Device{

    public Projector(int costOfDevice) {
        super(costOfDevice);
    }

    @Override
    public void doDeviceWork() {
        System.out.println("Projector is working");
        this.initDevice();
    }

}
