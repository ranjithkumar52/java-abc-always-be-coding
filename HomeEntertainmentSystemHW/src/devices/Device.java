package devices;


public abstract class Device{
    int costOfDevice;
    boolean isDeviceOn = false;  
    boolean isConnectedToRemote = false; 
    
    Device(int costOfDevice){
        this.costOfDevice = costOfDevice;
    }
    
    abstract public void doDeviceWork();
    public void initDevice(){
        this.isConnectedToRemote = true;
        this.isConnectedToRemote = true;
    }
    
}
