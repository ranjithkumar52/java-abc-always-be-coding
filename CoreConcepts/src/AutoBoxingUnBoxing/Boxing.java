package AutoBoxingUnBoxing;

import java.util.ArrayList;

public class Boxing {

    public static void doPracticeBoxing() {
        //how to declare an arraylist of type primitive types?
        //ArrayList<int> arrayListIntegeres = new ArrayList();
        ArrayList<Integer> arrayListIntegers = new ArrayList<Integer>();
        arrayListIntegers.add(1);
        arrayListIntegers.add(2);
        arrayListIntegers.add(3);
        arrayListIntegers.add(4);
        arrayListIntegers.add(5);
        int[] intArray = new int[arrayListIntegers.size()];
        for(int i=0; i<intArray.length; i++){
            intArray[i] = arrayListIntegers.get(i);
        }
        for(int i: intArray)
            System.out.println(i);
    }
}
