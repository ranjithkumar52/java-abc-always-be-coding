package General;

import java.util.Scanner;

public class StaticPractice {

    private static boolean flag;
    private static int B;
    private static int H;
    private static Scanner sc = new Scanner(System.in);

    static {
        try {
            B = sc.nextInt();
            H = sc.nextInt();
            if (B >= 0 && H >= 0) {
                flag = true;
            } else {
                throw new Exception();
            }
        }
        catch(Exception e){
            System.out.println("java.lang.Exception: Breadth and height must be positive");
        }

    }

    public static void main(String[] args) {
        if (flag) {
            int area = B * H;
            System.out.print(area);
        }
        

    }//end of main

}
