package General;

import java.util.Scanner;

public class NestedLogic {

    public static void main(String args[]) {
        int actDate, actMonth, actYear, expDate, expMonth, expYear;
        int fine = 0;
        Scanner sc = new Scanner(System.in);
        //actual Scan
        actDate = sc.nextInt();
        actMonth = sc.nextInt();
        actYear = sc.nextInt();
        sc.nextLine();
        //exp
        expDate = sc.nextInt();
        expMonth = sc.nextInt();
        expYear = sc.nextInt();
        sc.close();
        //display
        //System.out.println(actualDate+" "+actualMonth+" "+actualYear+"\n"+expDate+" "+expMonth+" "+expYear);
        
        if (actYear == expYear) {
            if (actMonth < expMonth) {
                fine = 0;
            } else if (actMonth == expMonth) {
                if (actDate <= expDate) {
                    fine = 0;
                } else {
                    fine = 15 * (actDate - expDate);
                }
            } else  {//if (actMonth > expMonth)
                fine = 500 * (actMonth - expMonth);
            }
        } else if (actYear < expYear) {
            fine = 0;
        } else { //act year > expYear
            fine = 10000;
        }

    }

}
