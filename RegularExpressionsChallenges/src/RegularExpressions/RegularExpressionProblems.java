package RegularExpressions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class RegularExpressionProblems {

    public void regularExpProb1() {
        Scanner sc = new Scanner(System.in);
        int rows = sc.nextInt();
        String firstName;
        String email;
        ArrayList<String> stringArrayList = new ArrayList<>();

        for (int i = 0; i < rows; i++) {
            //read one line of input
            firstName = sc.next();
            email = sc.next();
            //check if the second part of each line contains [\w][@][gmail.com]
            RegexTestHarness obj = new RegexTestHarness();
            if (obj.regexCheckGmail("[\\w][@][gmail.com]", email)) {
                stringArrayList.add(firstName);
            }
            //if true store the first part of input line into an String arraylist
        }
        //finally print all the elements from the arraylist
        Collections.sort(stringArrayList);
        //after sorting
        System.out.println("After Sorting:");
        for (String counter : stringArrayList) {
            System.out.println(counter);
        }

    }
}
