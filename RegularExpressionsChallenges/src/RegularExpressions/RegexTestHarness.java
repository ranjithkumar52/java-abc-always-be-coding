package RegularExpressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTestHarness {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your regular expression");
        String theRegex = sc.nextLine();
        System.out.println("Enter your input string to check");
        String str2Check = sc.nextLine();
        regexCheck(theRegex, str2Check); //remove static in the method declaration later
        //System.out.println("theRegex: "+theRegex+"\nstr2Check: "+str2Check);
        //System.out.println("theRegex: "+theRegex);
        
    }
    public static void regexCheck(String theRegex, String str2Check){
        Pattern checkRegex = Pattern.compile(theRegex);//creates a regular expression
        Matcher regexMatcher = checkRegex.matcher(str2Check);//perform match operations
        while(regexMatcher.find()){
            if(regexMatcher.group().length() != 0){ //kicking out non-empty strings
                System.out.printf("I found the text" + " \"%s\" starting at " + "index %d and ending at index %d.%n",
                    regexMatcher.group(),
                    regexMatcher.start(), //inclusive of start index
                    regexMatcher.end()); //excluse of end idex //trim removes white spaces
            }
        }
        
    }
    public boolean regexCheckGmail(String theRegex, String str2Check){//[\\w][@gmail\\.com]
        Pattern checkRegex = Pattern.compile(theRegex);//creates a regular expression
        Matcher regexMatcher = checkRegex.matcher(str2Check);//perform match operations
        while(regexMatcher.find()){
            if(regexMatcher.group().length() != 0){ //kicking out non-empty strings
                System.out.printf("I found the text" + " \"%s\" starting at " + "index %d and ending at index %d.%n",
                    regexMatcher.group(),
                    regexMatcher.start(), //inclusive of start index
                    regexMatcher.end()); //excluse of end idex //trim removes white spaces
            }
            return true;
        }
      return false; 
    }
}
