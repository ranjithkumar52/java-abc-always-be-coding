package Stacks.ADT;


import java.util.Scanner;

public class StackSimpleArray {

    //members variables
    private int top;
    private int[] stackArray;

    /*
    Constructor
     */
    public StackSimpleArray() throws ArrayIndexOutOfBoundsException {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Enter the size of the Stack");
            int stackSize = sc.nextInt();
            stackArray = new int[stackSize];
            top = 0;
        }

    }

    /*
    setters and getters
     */
    public void settop(int top) {
        this.top = top;
    }

    public void setstackArray(int[] stackArray) {
        this.stackArray = stackArray;
    }

    public int gettop() {
        return this.top;
    }

    public int[] getstackArray() {
        return this.stackArray;
    }

    /*
    *stack push
    *check for the size - overflow and underflow
    * think of the ways the methods can fail
     */
    public void stackPush(int element) throws ArrayIndexOutOfBoundsException {
        //overflow
        if (stackIsFull()) { //-1 because top is based on 0-based index
            System.out.println("Do you want to increase the stack size");
            try (Scanner sc = new Scanner(System.in)) {
                char check = sc.next().toLowerCase().charAt(0);
                switch (check) {
                    case 'y':
                        stackIncrementSize();
                        stackArray[top++] = element;
                        stackDisplay();
                        break;
                    case 'n':
                        break;
                    default:
                        System.out.println("Invalid character. Please enter 'y' to increase the stack size or 'n' to stop ");
                        this.stackPush(element);
                        break;
                }
            }

        } else {
            stackArray[top++] = element;
            stackDisplay();
            
            //[a, b, c]
        }
    }

    /*
    stack pop - check for underflow
     */
    public int stackPop() throws ArrayIndexOutOfBoundsException {
        //underflow
        if (stackIsEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Stack Underflow");
        } else {
            return stackArray[--top];
        }
    }

    public int stackSize() {
        return stackArray.length;
    }

    public void stackDisplay() {
        System.out.println("Elements in the stack");
        for(int i=0; i<=top; i++){
            if(i <= top){
                System.out.print(stackArray[i]+" ");
            }else{
                System.out.println();
            }
        }
        System.out.println("Top value: " + gettop());
    }

    public boolean stackIsEmpty() {
        return top == 0;
    }

    public boolean stackIsFull() {
        return top == stackArray.length;
    }

    public void stackIncrementSize() {
        System.out.println("Before increment: " + this.stackSize());
        //create an array
        int[] oldArray = getstackArray();
        int[] newArray = new int[getstackArray().length + 1];
        //copy the elements into the old
        for (int i : oldArray) {
            newArray[i] = oldArray[i];
        }
        setstackArray(newArray);
        System.out.println("After increment: " + this.stackSize());

    }

}
