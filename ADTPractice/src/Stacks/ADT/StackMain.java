package Stacks.ADT;

public class StackMain {

    public static void main(String args[]) {
        try {
            StackSimpleArray stack1 = new StackSimpleArray();

            stack1.stackPush(1);

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("check the array indices " + e);
        } catch (Exception e) {
            System.out.println("Failed to catch specific exception " + e);
        }

    }
}
