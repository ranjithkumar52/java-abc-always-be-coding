package Queue_ADT;

public class SimpleQueueArray {

    //Instance Variables
    private int[] queueArray;
    private int front;
    private int rear;
    private int capacity;
    private int numberOfElements;

    //Constructors
    public SimpleQueueArray(int size) {
        front = -1; //0-based
        rear = -1; //0-based
        capacity = size;
        queueArray = new int[size];
        numberOfElements = 0;
    }

    //Main methods
    //enQueue(int data)
    //Inserts an element at the end of the Queue 
    //Careful of Full Queue Exception
    public void enQueue(int data) throws Exception {
        if (isQueueFull()) {
            throw new Exception("Full Queue Exception");
        } 
        else if(isQueueEmpty()){
            queueArray[++rear] = data;
            numberOfElements++;
            front++;
        }
        
        else {
            queueArray[++rear] = data;
            numberOfElements++;
        }
    }

    //int deQueue()
    //Removes and returns the elements at the front of the queue
    //Careful of Empty Queue Exceptions
    public int deQueue() throws Exception {
        if (isQueueEmpty()) {
            throw new Exception("Empty Queue Exception");
        } else {
            numberOfElements--;
            return queueArray[front++];
        }
    }

    //Auxiliary Operations
    //int Front(); 
    //returns the element at the front without removing it
    public int getFront() throws Exception {
        if (isQueueEmpty()) {
            throw new Exception("Empty Queue Exception");
        } else {
            return queueArray[front];
        }
    }

    //int QueueSize(); 
    //Returns the number of elements stored in the queue
    public int getQueueSize() {
        return numberOfElements;
    }

    //boolean isEmpty(): 
    //Indicate whether no elements are stored in the queue or not
    public boolean isQueueEmpty() {
        if (front == -1 || front == capacity) {
            return true;
        } else {
            return false;
        }
    }

    //to check if the queue is full
    public boolean isQueueFull() {
        if (rear == capacity - 1) {
            return true;
        } else {
            return false;
        }
    }
    
    public void displayQueue(){
        if(isQueueEmpty()){
            System.out.println("Empty Queue");
        }else{
            System.out.println("Elements in the Queue");
            for(int i=front; i<=rear; i++){
                System.out.print(queueArray[i]+" ");
            }
            System.out.println();
        }
    }

}
