package Queue_ADT;

public class SimpleQueueArrayTest {

    public static void main(String args[]) {
        try {
            //test for no-arg constructor
            SimpleQueueArray queue1 = new SimpleQueueArray(10);
            //check every methods functionality
            for(int i=0; i<10; i++)
                queue1.enQueue(i);
            queue1.displayQueue();
            
            int count = 10;
            System.out.println("Element Dequeued:");
            while(count > 0){
                System.out.print(queue1.deQueue()+" ");
                count--;
            }
            System.out.println();
            queue1.displayQueue();
            
            //draw back of simplequeue - all the empty spaces formed after dequeue is a waste.
            //because of this, we have to implement queues using circular arrays
            for(int i=0; i<10; i++)
                queue1.enQueue(i);
            queue1.displayQueue();
            
            count = 10;
            System.out.println("Element Dequeued:");
            while(count > 0){
                System.out.print(queue1.deQueue()+" ");
                count--;
            }
            System.out.println();
            queue1.displayQueue();
           
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }

    }
}
