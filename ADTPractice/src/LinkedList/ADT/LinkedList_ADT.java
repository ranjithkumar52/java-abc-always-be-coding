package LinkedList.ADT;

public class LinkedList_ADT {

    Node head;

    public LinkedList_ADT() {
        this.head = null;
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void insertElementsAtTheEnd(int data) {
        //empty list
        if (head == null) {
            head = new Node(data);
        } else {
            Node current = head;
            Node newNode = new Node(data);
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(newNode);

        }
    }

    public void insertElementAtTheBeginning(int data) {
        if (head == null) {
            head = new Node(data); // no need i guess; check this method again
        } else {
            Node current = head;
            Node temp = new Node(data);
            temp.setNext(head);
            this.setHead(temp);
        }
    }

    public void insertElementAtPosition(int data, int position) {
        int size = this.size();
        if (position <= 0 || position > size) {
            System.out.println("Invalid position of the list. Please enter position between 1 and " + size);
        } else if (position == 1) {
            insertElementAtTheBeginning(data);
        } else {
            Node current = head;
            Node temp = new Node(data);

            for (int i = 1; i < position - 1; i++) //for the node before the position
            {
                current = current.getNext();
            }

            Node positionNode = current; //node before the position node; the position before new node is inserted
            Node positionNextNode = positionNode.getNext();
            positionNode.setNext(temp);
            temp.setNext(positionNextNode);

        }
    }

    public void display() {
        System.out.print("Elements in the linked list: ");
        if (head == null) {
            System.out.println("Empty list");
        } else {
            Node current = head;
            while (current != null) {
                System.out.print(" " + current.getData());
                current = current.getNext();
            }
            System.out.println();

        }
    }

    public int size() {
        int count = 0;
        if (head == null) {
            return -1;
        } else {
            Node current = head;
            while (current != null) {
                count++;
                current = current.getNext();
            }
            return count;
        }
    }

    public void deleteNodeAtTheBeginning() {
        Node temp = head;
        head = head.getNext();
        temp.setNext(null);

    }

    public void deleteNodeAtTheEnd() {
        int size = this.size();
        Node currentNode = head;
        Node nextNode;
        if (size == 1) {
            head = null;
        }
        else if(size == 0){
            System.out.println("Can't delete from an empty list");
        }
        else {
            for (int i = 1; i < size - 1; i++) { //min value i: 3
                currentNode = currentNode.getNext();
            }
            nextNode = currentNode.getNext();
            nextNode = null;
            currentNode.setNext(null);
        }

    }

}
