
package LinkedList.ADT;

public class LinkedListMain {

    public static void main(String[] args) {
        LinkedList_ADT trainCar = new LinkedList_ADT(); 
        
        for(int i=0; i<10; i++)
            trainCar.insertElementsAtTheEnd(i);
        
        trainCar.display();
        
        for(int i=0; i<12; i++)
            trainCar.deleteNodeAtTheEnd();
        
        trainCar.display();
        
    }
    
}
