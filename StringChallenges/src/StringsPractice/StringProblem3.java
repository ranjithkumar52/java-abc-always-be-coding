package StringsPractice;

import java.util.Scanner;

public class StringProblem3 {

    public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";

        if (k <= s.length()) {
            //print k length substring
            for (int i = 0; i + k <= s.length(); i++) {

                if (i == 0) {
                    smallest = s.substring(i, i + k);
                    largest = smallest;
                } else if (s.substring(i, i + k).compareTo(smallest) < 0) {
                    smallest = s.substring(i, i + k);
                } else if (s.substring(i, i + k).compareTo(largest) > 0) {
                    largest = s.substring(i, i + k);
                }

            }

            return smallest + "\n" + largest;
        } else {
            return s;
        }

    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int k = scan.nextInt();
        scan.close();

        System.out.println(getSmallestAndLargest(s, k));
    }

}
