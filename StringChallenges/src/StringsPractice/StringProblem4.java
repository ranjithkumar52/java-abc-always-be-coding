package StringsPractice;

import java.util.Scanner;

public class StringProblem4 {

    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        char[] charArray = s.toCharArray();
        char[] charReverse = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            charReverse[s.length() - i - 1] = charArray[i];
        }
        String reverse = new String(charReverse);
        System.out.println(s.equals(reverse) ? "Yes" : "No");
    }
}
//later modify this code with string builder class