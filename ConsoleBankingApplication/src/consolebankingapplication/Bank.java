package consolebankingapplication;

import java.util.ArrayList;

public class Bank {

    public String bankName;
    private ArrayList<Branch> branches;

    public Bank(String bankName) {
        this.bankName = bankName;
        branches = new ArrayList<Branch>();
    }
    
    public void addNewBranch(Branch newBranch){
        this.getBranches().add(newBranch);
    }
    
    //Getters and setters
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public ArrayList<Branch> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<Branch> branches) {
        this.branches = branches;
    }
    
    public void listAllBranches(){
        System.out.println("Listing all the branches under the bank: "
                + getBankName()+".....");
        for (Branch branchIndex : branches) {
            System.out.println(branchIndex.getBranchName());
        }
    }
    
    
    
}