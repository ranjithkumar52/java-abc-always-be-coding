package consolebankingapplication;

import java.util.ArrayList;

class Branch {

    private String branchName;
//    private ArrayList<Customer> customers = new ArrayList<Customer>(); 
//    objects are runtime concepts so creating them here doesn't make sense
    private ArrayList<Customer> customers;

    Branch(String branchName) {
        this.branchName = branchName;
        customers = new ArrayList<>();
    }

    public void addNewCustomer(Customer newCustomer) {
        if (checkCustomerExist(newCustomer)) {
            System.out.println("Customer " + newCustomer.getCustomerName() + " already exists");
        } else {
            this.getCustomers().add(newCustomer);
        }

    }

    private boolean checkCustomerExist(Customer newCustomer) {
        return this.getCustomers().contains(newCustomer);
    }

    //setters and getters
    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public void listAllCustomers() {
        System.out.println("Listing all the customers for the branch: " + getBranchName()
                + "....");
        for (Customer customer : customers) {
            System.out.println("Customer: " + customer.getCustomerName());
        }

    }

}
