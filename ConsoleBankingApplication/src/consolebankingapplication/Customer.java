package consolebankingapplication;

import java.util.ArrayList;

class Customer {

    private String customerName;
    private ArrayList<Double> transactions = new ArrayList<Double>();

    Customer(String customerName, double newTransaction) {
        this.customerName = customerName;
        addTransactions(newTransaction);
    }

    public void addTransactions(double newTransaction) {
        this.getTransactions().add(newTransaction);
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public ArrayList<Double> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<Double> transactions) {
        this.transactions = transactions;
    }

    public void listAllTransactions() {
        System.out.println("Showing all transactions for Name: " + getCustomerName() + "...");
        for (Double transactionsIndex : transactions) {
            System.out.println(transactionsIndex.doubleValue());
        }
        System.out.println();
    }
}
